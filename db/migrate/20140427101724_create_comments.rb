class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :content
      t.string :comment_type
      t.integer :comment_id

      t.timestamps
    end
  end
end
