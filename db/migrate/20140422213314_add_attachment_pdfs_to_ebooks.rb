class AddAttachmentPdfsToEbooks < ActiveRecord::Migration
  def self.up
    change_table :ebooks do |t|
      t.attachment :pdfs
    end
  end

  def self.down
    drop_attached_file :ebooks, :pdfs
  end
end
