class CreateEbooks < ActiveRecord::Migration
  def change
    create_table :ebooks do |t|
      t.string :title
      t.string :author
      t.text :review
      t.text :description
      t.references :user, index: true

      t.timestamps
    end
  end
end
