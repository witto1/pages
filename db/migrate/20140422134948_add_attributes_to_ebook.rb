class AddAttributesToEbook < ActiveRecord::Migration
  def change
    add_column :ebooks, :active, :boolean
    
    add_column :ebooks, :paid, :boolean
    
    add_column :ebooks, :downloaded, :integer
  end
end
