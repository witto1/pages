class Ebook < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  belongs_to :user
   has_attached_file :pdfs
  validates_attachment :pdfs, content_type: { content_type: "application/pdf" }

validates :title, :author, presence: true
validates :review, length: {minimum: 150}, allow_blank: true
validates :description, length: {minimum: 50}, presence: true


 belongs_to :user
 before_create :set_downloaded

  has_attached_file :cover, :styles => { :medium => "300x300>", :thumb => "100x100>", :cropped => "200" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :cover, :content_type => /\Aimage\/.*\Z/

default_scope -> {order'created_at DESC'}

 self.per_page = 6
 after_validation(on: :create) do
self.active = false
end


def self.search(search)
  if search
    find(:all, :conditions => ['title LIKE ? OR author LIKE ?', "%#{search}%", "%#{search}%"])
  else
    find(:all)
  end
end

scope :active, ->{where active:true}
scope :inactive, ->{where active:false}

scope :pay, ->{where paid:true}
scope :free, ->{where paid:false}


private

  def set_downloaded
    self.downloaded = 0
  end




end
