json.array!(@ebooks) do |ebook|
  json.extract! ebook, :id, :title, :author, :review, :description, :user_id
  json.url ebook_url(ebook, format: :json)
end
