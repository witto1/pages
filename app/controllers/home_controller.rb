class HomeController < ApplicationController
  def index
  	 @ebooks = Ebook.active.search(params[:search])
  end
end
