require 'will_paginate/array' 
  class EbooksController < ApplicationController

  before_action :set_ebook, only: [:show, :edit, :update, :destroy,:download_pdf]
  before_action :authenticate_user!, only: [:create,:my]
  before_action :set_update_downloaded, only: [:download_pdf]

  def index
    @ebooks = Ebook.active.paginate(:page => params[:page]).search(params[:search])
  end
  def my
    @ebooks = current_user.ebooks.paginate(:page =>params[:page])
    
  end
  def inactive
    @ebooks = Ebook.inactive.paginate(:page =>params[:page])
    render :index
  end
  def lastpaid
    @ebooks = Ebook.active.pay.last(6).paginate(:page =>params[:page])
    render :index
  end
  def lastfree
    @ebooks = Ebook.active.free.last(6).paginate(:page =>params[:page])
    render :index
  end  
  def home
    @ebooks = Ebook.last(6).paginate(:page =>params[:page])
  end
  def download_pdf
  redirect_to root_url[0..-2] + @ebook.pdfs.url
  end



  def toppaid
    @ebooks = Ebook.active.pay.order(:downloaded).last(6).paginate(:page =>params[:page])
    render :index
  end
  def topfree
    @ebooks = Ebook.active.free.order(:downloaded).last(6).paginate(:page =>params[:page])
    render :index
  end


  # GET /ebooks/1
  # GET /ebooks/1.json
  def show
 end

  # GET /ebooks/new
  def new
    @ebook = Ebook.new
  end

  # GET /ebooks/1/edit
  def edit
  end

  # POST /ebooks
  # POST /ebooks.json
  def create

    @ebook = Ebook.new(ebook_params)
    @ebook.pdfs = params[:ebook][:pdfs]
    
    

    @ebook= current_user.ebooks.new(ebook_params)


    respond_to do |format|
      if @ebook.save
        format.html { redirect_to @ebook, notice: 'Ebook was successfully created.' }
        format.json { render action: 'show', status: :created, location: @ebook }
      else
        format.html { render action: 'new' }
        format.json { render json: @ebook.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ebooks/1
  # PATCH/PUT /ebooks/1.json
  def update
    respond_to do |format|
      if @ebook.update(ebook_params)
        format.html { redirect_to @ebook, notice: 'Ebook was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ebook.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ebooks/1
  # DELETE /ebooks/1.json
  def destroy
    @ebook.destroy
    respond_to do |format|
      format.html { redirect_to ebooks_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ebook
      @ebook = Ebook.find(params[:id])
    end
    def reverse_order
      @ebooks = Ebook.reverse_order
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def ebook_params
      params.require(:ebook).permit(:title, :author, :review, :description, :user_id, :cover, :pdfs)
    end

    def set_update_downloaded
      @ebook.update_attributes(downloaded: @ebook.downloaded+1)
    end
  end
